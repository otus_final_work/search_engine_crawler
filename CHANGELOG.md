# Список изменений:
1. Написан Dockerfile
2. Написан helm-chart
3. Написан конфиг пайплайна gitlab-ci
4. В helm-chart добавлен конфиг для мониторинга сервиса
5. В .gitlab-ci.yml добавил правила для пересборки docker-image