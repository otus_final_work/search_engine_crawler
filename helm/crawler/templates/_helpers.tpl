{{- define "crawler.fullname" -}}
{{- printf "%s-%s" .Release.Name .Chart.Name}}
{{- end -}}
{{- define "crawler.servicemon" -}}
{{- printf "%s-%s-%s" .Release.Name .Chart.Name "servicemonitor" }}
{{- end -}}
{{- define "rabbit.fullname" -}}
{{- printf "%s-%s" .Release.Name "rabbitmq"}}
{{- end -}}
{{- define "rabbit.service" -}}
{{- printf "%s-%s" .Release.Name .Values.rmq.saName}}
{{- end -}}